﻿var userName;
$(function () {
   
    log('Requesting Capability Token...');
    $.getJSON('/token')
      .done(function (data) {
          log('Got a token.');
          console.log('Token: ' + data.token);
          userName=data.clientName
          
          // Setup Twilio.Device
          Twilio.Device.setup(data.token);

          Twilio.Device.ready(function (device) {
              log('Twilio.Device Ready!');
             // document.getElementById('call-controls').style.display = 'block';
          });

          Twilio.Device.error(function (error) {
             // log('Twilio.Device Error: ' + error.message);
              showCallButton();
          });

          Twilio.Device.connect(function (conn) {
              //document.getElementById('button-call').style.display = 'none';
             // document.getElementById('button-hangup').style.display = 'inline';
              log('Successfully established call!');
          });

          Twilio.Device.disconnect(function (conn) {
              document.getElementById('phone-number').value = '';
              document.getElementById('phone-number').placeholder = 'Enter Number';
              log('Call ended.');
              showCallButton();
          });

          Twilio.Device.incoming(function (conn) {
              log('Incoming connection from ' + conn.parameters.From);
              var archEnemyPhoneNumber = '+12099517118';

              if (conn.parameters.From === archEnemyPhoneNumber) {
                  conn.reject();
                  log('It\'s your nemesis. Rejected call.');
              } else {
                  // accept the incoming connection and start two-way audio
                  conn.accept();
                  document.getElementById('phone-number').value = conn.parameters.From;
                  showHangupButton();
              }
          });
          setClientNameUI(data.identity);
          setNumberOfCalls();
          setInterval(setNumberOfCalls, 5000);
      })
      .fail(function () {
          log('Could not get a token from server!');
      });
    // Bind button to make call
    document.getElementById('button-answer').addEventListener("click", answerCall);
    function answerCall() {
        // get the phone number to connect the call to
        showHangupButton();
        var params = {
            To: '',
            From: userName
        };
        console.log(userName);
        console.log('Calling ' + params.To + '...');
        Twilio.Device.connect(params);
    }
    document.getElementById('button-call').addEventListener("click", makeCall);
    function makeCall() {
        debugger;
            // get the phone number to connect the call to
        showHangupButton();
        var params = {
            To: document.getElementById('phone-number').value,
            From:userName
        };
        console.log(userName);
        console.log('Calling ' + params.To + '...');
        Twilio.Device.connect(params);
    }

    //document.getElementById('button-status').addEventListener("click", calls);


    // Bind button answerButtonClick



    // Bind button to hangup call
    document.getElementById('button-hangup').addEventListener("click", hangupFunction);
    function hangupFunction() {
        log('Hanging up...');
        Twilio.Device.disconnectAll();
        showCallButton();
        
    }

    //Binding Call's Number In Queue Information
    

});

// Activity log
function log(message) {
    //var logDiv = document.getElementById('log');
    //logDiv.innerHTML += '<p>&gt;&nbsp;' + message + '</p>';
    //logDiv.scrollTop = logDiv.scrollHeight;
}

// Set the client name in the UI
function setClientNameUI(clientName) {
    //var div = document.getElementById('client-name');
    //div.innerHTML = 'Your client name: <strong>' + clientName +      '</strong>';
    userName = clientName;
    console.log(userName);
}

// Get Call's length and setting in ..
function setNumberOfCalls() {
    $.ajax({
        url: "http://localhost:50010/api/queue/getCallsInQueue",
        contentType: "application/json",
        dataType: 'json',
        success: function (result) {
            if (result >= 1) {
                var CallsNumberInQueue = document.getElementById('waitingCalls');
                CallsNumberInQueue.innerHTML = result;
                var diplay = true;
                showAnswerButton(diplay);
                incomingCallNumber();
                //document.getElementById('button-call').style.display = "none";
                console.log(result);
            }
            else {
                var CallsNumberInQueue = document.getElementById('waitingCalls');
                CallsNumberInQueue.innerHTML = result;
                var diplay = false;
                var numberTextbox = document.getElementById('phone-number');
                numberTextbox.value = "";

              //  showAnswerButton(diplay);
               // document.getElementById('button-call').style.display = "none";
                console.log(result);
            }
        }

    });
}

//Getting number of incoming call
function incomingCallNumber() {
    $.ajax({
        url: "http://localhost:50010/api/queue/getNumberOfConectedCall",
        contentType: "application/json",
        dataType: 'json',
        success: function (result) {
            if (result != null) {
                var numberTextbox = document.getElementById('phone-number');
                numberTextbox.value = result;
                console.log(result);
            }
            else {
                var numberTextbox = document.getElementById('phone-number');
                numberTextbox.value ="enter number";
                console.log(result);
            }
        }

    });
}

// Clear button logic
$('.row a').on('click', function () {
    debugger;
    var buttonValue = $(this.text);
    if (buttonValue.selector != "c") {
        if (buttonValue.selector != "Dial" && buttonValue.selector != "Hangup" && buttonValue.selector != "Answer Calls in Queue 0") {
            var $store = $('input'),
                   $number = $('.call'),
                   dialed = $(this).text();

            $store.val($store.val() + dialed);
            $number.attr('href', 'tel:' + $store.val());
            $store.focus();
        }
    }
    else {
        var textVal = $('input').val();
        $('input').val(textVal.substring(0, textVal.length - 1));
    }
});
function showAnswerButton(display) {
    if(display===true)
    {
        document.getElementById('button-answer').style.display = "block";
    }
    else {
        document.getElementById('button-answer').style.display = "none";
    }
}
function showCallButton() {
    document.getElementById('button-hangup').style.display = "none";
    document.getElementById('button-call').style.display = "block";
}
function showHangupButton() {
    document.getElementById('button-call').style.display = "none";
    document.getElementById('button-hangup').style.display = "block";
}
//calling api to make call
//function callapi() {
//    var xhttp = new XMLHttpRequest();
//    xhttp.onreadystatechange = function () {
//        if (this.readyState == 4 && this.status == 200) {
//            alert("data is saved");
//        }
//    };

//    xhttp.open("GET", "https://5bd43ce7.ngrok.io/Api/CallStatus/GetCallStatus?par=" + document.getElementById('phone-number').value + "", true);
//    xhttp.send();
//}


//setTimeout(function () { callapi(); }, 5000);


//var statusCallbackEvent =  [
//    "completed",
//    "ringing"
//];
//debugger;
//var params = {

//        //statusCallback: new Uri("https://5bd43ce7.ngrok.io/Api/CallStatus/GetCallStatus"),
//        //statusCallbackEvent: statusCallbackEvent,
//        //statusCallbackMethod: Twilio.Http.HttpMethod.Post,
//        To: document.getElementById('phone-number').value
//    };

//    console.log('Calling ' + params.To + '...');
//    Twilio.Device.connect(params);
//document.getElementById('button-hangup').onclick = function () {
//    document.getElementById('button-hangup').style.display = "none";
//    document.getElementById('button-call').style.display = "block";
//    log('Hanging up...');
//    Twilio.Device.disconnect();
//    Twilio.Device.ready();

//};
//    onclick = function () {
//    // get the phone number to connect the call to

//    var params = {
//        To: document.getElementById('phone-number').value
//    };

//    console.log('Calling ' + params.To + '...');
//    Twilio.Device.connect(params);
//};