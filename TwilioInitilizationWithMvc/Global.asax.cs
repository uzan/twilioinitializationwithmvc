﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Http;
using Twilio;
using Twilio.Rest.Api.V2010.Account;

namespace TwilioInitilizationWithMvc
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            GlobalConfiguration.Configure(WebApiConfig.Register);
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
           // CreateQueue();

        }
        public void CreateQueue()
        {
            const string accountSid = "ACc5375fdc5addc508794324784dd0b790";
            const string authToken = "43744100b0ca18503eb495a5394b4ef8";
            TwilioClient.Init(accountSid, authToken);
            var queue = QueueResource.Create(friendlyName: "Complain",pathAccountSid:accountSid,maxSize:100);
        }
    }
}
