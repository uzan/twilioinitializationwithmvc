﻿using System.Collections.Generic;
using System.Configuration;
using System.Web.Mvc;
using Twilio.Jwt;
using Twilio.Jwt.Client;



namespace TwilioInitilizationWithMvc.Controllers
{
    public class TokenController : Controller
    {
        // GET: Token
        public ActionResult Index()
        {
            // Load Twilio configuration from Web.config
            var accountSid = ConfigurationManager.AppSettings["TwilioAccountSid"];
            var authToken = ConfigurationManager.AppSettings["TwilioAuthToken"];
            var appSid = ConfigurationManager.AppSettings["TwilioTwimlAppSid"];

            // Create a random identity for the client
            var identity = "Uzain";

            // Create an Access Token generator
            var scopes = new HashSet<IScope>
          {
              { new IncomingClientScope(identity) },
              { new OutgoingClientScope(appSid) }
          };
            var capability = new ClientCapability(accountSid, authToken, scopes: scopes);
            var token = capability.ToJwt();

            return Json(new
            {
                identity,
                token
            }, JsonRequestBehavior.AllowGet);
        }
    }
}