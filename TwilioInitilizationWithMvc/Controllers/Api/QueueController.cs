﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Twilio;
using Twilio.Rest.Api.V2010.Account;
using Twilio.Rest.Api.V2010.Account.Queue;

namespace TwilioInitilizationWithMvc.Controllers.Api
{
    public class QueueController : ApiController
    {
        
        public int?  getCallsInQueue()
        {
            const string accountSid = "ACc5375fdc5addc508794324784dd0b790";
            const string authToken = "43744100b0ca18503eb495a5394b4ef8";

            TwilioClient.Init(accountSid, authToken);
            var queue = QueueResource.Fetch(pathSid: "QUcea7afbe3ee14c8588761cbb68dd1fb7");
            return queue.CurrentSize;
        }
        public string getNumberOfConectedCall()
        {
            const string accountSid = "ACc5375fdc5addc508794324784dd0b790";
            const string authToken = "43744100b0ca18503eb495a5394b4ef8";

            TwilioClient.Init(accountSid, authToken);
            var queue = QueueResource.Fetch(pathSid: "QUcea7afbe3ee14c8588761cbb68dd1fb7");
            Console.WriteLine(queue.CurrentSize);

            var queueMember = MemberResource.Read(
                pathQueueSid: "QUcea7afbe3ee14c8588761cbb68dd1fb7",
                        limit: 1
                        );
            CallResource call = null;
            foreach (var callin in queueMember)
            {
                if (callin.Position == 1)
                {
                    call = CallResource.Fetch(pathSid: callin.CallSid);
                    
                    break;
                }
                else
                    continue;
            }
            if (call.From != null)
                return call.From;
            else
                return "";
            
        }
    }
}
