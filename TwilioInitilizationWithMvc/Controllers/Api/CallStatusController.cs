﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Twilio;
using Twilio.Rest.Api.V2010.Account;
using TwilioInitilizationWithMvc.Models;
using System.Web;
using Twilio.Http;
using System.Globalization;
using Twilio.TwiML;
using System.Text;

namespace TwilioInitilizationWithMvc.Controllers.Api
{
    public class DialedCall
    {
        public string DialCallStatus { get; set; }
        public string DialCallSid { get; set; }
        public string DialCallDuration { get; set; }
        public string RecordingUrl { get; set; }
    }

    [System.Web.Http.RoutePrefix("Api/CallStatus")]
    public class CallStatusController : ApiController
    {
        private readonly MySQLDBClass dbmysql;
        public CallStatusController()
        {
            dbmysql = new MySQLDBClass();        
        }
        [HttpPost]
        [Route("GetCallStatusInbound")]
        public IHttpActionResult GetCallStatusInbound(RingingCalls callinfo)
        {
            string IP = ((HttpContextBase)Request.Properties["MS_HttpContext"]).Request.UserHostAddress;           
            var CallSid = callinfo.CallSID;
            InsertOrDeleteAccordingToStatus(CallSid, IP);
            return Ok();

        }
        
        [HttpPost]
        [Route("GetCallStatusOutbound")]
        public HttpResponseMessage GetCallStatusOutbound(DialedCall callinfo)
        {
            string IP = ((HttpContextBase)Request.Properties["MS_HttpContext"]).Request.UserHostAddress;
            //var CallSid = callinfo.CallSID;
            var CallSid = callinfo.DialCallSid;
            InsertOrDeleteAccordingToStatus(CallSid, IP);
            var result = new StringBuilder("<Response/>");
            var res = Request.CreateResponse(HttpStatusCode.OK);
            res.Content = new StringContent(result.ToString(), Encoding.UTF8, "text/xml");

            return res;


        }
        public void InsertOrDeleteAccordingToStatus(string CallSid,string IP) {
            const string accountSid = "ACc5375fdc5addc508794324784dd0b790";
            const string authToken = "43744100b0ca18503eb495a5394b4ef8";

            TwilioClient.Init(accountSid, authToken);
            var call = CallResource.Fetch(
                      pathSid: CallSid
                      );
           
                var startTime = call.StartTime.Value.ToString("yyyy-M-dd hh:mm:ss");
                var endTime = call.EndTime.Value.ToString("yyyy-M-dd hh:mm:ss");
                var number = call.To;
                TimeSpan? ttime = call.EndTime - call.StartTime;
                int ts = Convert.ToInt16(ttime.Value.Seconds);
                double tm = Convert.ToDouble(ttime.Value.Minutes);
                var cmd = "DELETE FROM live_channels WHERE channel_data='" + (call.Direction != "outbound-dial"? call.From:call.To) + "';";
                dbmysql.MySQLDelete(cmd);
                var comd = "INSERT INTO call_log VALUES ('" + call.Sid + "','" + 1001 + "','" + call.Status.ToString() + "','" + "SIP" + "','" + IP + "','" + 1001 + "','" + call.To + "','" + call.From + "','" + startTime + "','" + 101 + "','" + endTime + "','" + 0 + "','" + ts + "','" + tm + "')";
                dbmysql.MySQLInsert(comd);
            
           
        }



    }
}
//if (call.Status.ToString() == "ringing")
//{

//    var cmmd = "INSERT INTO live_channels VALUES('" + 1001 + "','" + IP + "','" + call.Direction + "','" + 1001 + "','" + call.To + "');";
//    dbmysql.MySQLInsert(cmmd);
//}
//else
//{  }