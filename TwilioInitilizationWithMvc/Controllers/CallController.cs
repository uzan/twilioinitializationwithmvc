﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Twilio;
using Twilio.AspNet.Mvc;
using Twilio.Rest.Api.V2010.Account;
using Twilio.TwiML;
using TwilioInitilizationWithMvc.Models;
using TwilioInitilizationWithMvc.Controllers;
using System.Data.OleDb;
using MySql.Data;
using MySql.Data.MySqlClient;
using Twilio.TwiML.Voice;

namespace TwilioInitilizationWithMvc.Controllers
{
    public class CallController : TwilioController
    {
        private readonly MySQLDBClass dbmysql;
        private readonly ApplicationDbContext context;
        const string accountSid = "ACc5375fdc5addc508794324784dd0b790";
        const string authToken = "43744100b0ca18503eb495a5394b4ef8";
        public CallController()
        {
            dbmysql = new MySQLDBClass();
            context = new ApplicationDbContext();
        }
        [HttpPost]
        public ActionResult Index()
        {
           
            var response = new VoiceResponse();
            response.Say("Thank you for calling!.");
            // Use the <Gather> verb to collect user input
            //var gather = new Gather(action: Url.ActionUri("Show", "Menu"), numDigits: 1);
            //response.Gather(new Gather(numDigits: 1).Say("For sales, press 1. For support, press 2."));
            //If the user doesn't enter input, loop

            // response.Redirect(url:new Uri("https://4fafb018.ngrok.io/Api/Call"));
            //response.Append(gather);
            return TwiML(response);

        }
        public void methodd()
        {
            TwilioClient.Init(accountSid, authToken);

            var QueuedCalls = CallResource.Read(
                        status: CallResource.StatusEnum.Queued
                        );
            foreach (var call in QueuedCalls)
            {
                if (call.Status.ToString() == "queued")
                {
                    var callDetailRinging = new QueuedCalls()
                    {
                        CallTo = call.To,
                        CallSID = call.Sid,
                        CallFrom = call.From,
                        DateCreated = call.DateCreated,
                        DateUpdated = call.DateUpdated,
                        Duration = call.Duration,
                        StartTime = call.StartTime,
                        EndTime = null,
                        Direction = call.Direction,
                        Status = call.Status.ToString()
                    };
                    context.QueuedCalls.Add(callDetailRinging);
                }

            }
            /////////////////////////////////////////////////////////////////
            var calls = CallResource.Read(
                        status: CallResource.StatusEnum.Ringing
                        );

            foreach (var call in calls)
            {
                if (call.Status.ToString() == "ringing")
                {
                    var callDetailRinging = new RingingCalls()
                    {
                        CallTo = call.To,
                        CallSID = call.Sid,
                        CallFrom = call.From,
                        DateCreated = call.DateCreated,
                        DateUpdated = call.DateUpdated,
                        Duration = call.Duration,
                        StartTime = call.StartTime,
                        EndTime = null,
                        Direction = call.Direction,
                        Status = call.Status.ToString()
                    };
                    var cmd = "INSERT INTO table_name (CallSID,CallTo,CallFrom,Direction,DateCreated,Status)VALUES('" + callDetailRinging.CallSID + "','" + callDetailRinging.CallTo + "','" + callDetailRinging.CallFrom + "','" + callDetailRinging.Direction + "','" + callDetailRinging.DateCreated + "','" + callDetailRinging.Status + "'); ";
                    dbmysql.MySQLInsert(cmd);
                    context.RingingCalls.Add(callDetailRinging);






                }
                else
                {
                    var callDetail = new CallDetail()
                    {
                        CallTo = call.To,
                        CallSID = call.Sid,
                        CallFrom = call.From,
                        DateCreated = call.DateCreated,
                        DateUpdated = call.DateUpdated,
                        Duration = call.Duration,
                        StartTime = call.StartTime,
                        EndTime = call.EndTime,
                        Direction = call.Direction,
                        Status = call.Status.ToString()
                    };
                    context.CallDetail.Add(callDetail);
                }
            }

            context.SaveChanges();
        }
    }
}