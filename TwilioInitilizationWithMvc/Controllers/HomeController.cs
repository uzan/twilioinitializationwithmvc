﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Twilio;
using Twilio.Clients;
using Twilio.Rest.Api.V2010.Account;
using TwilioInitilizationWithMvc.Models;
using TwilioInitilizationWithMvc.ViewModel;
using Twilio.Types;
using Twilio.TwiML;
using Twilio.AspNet.Mvc;
using System.Data;
using Twilio.AspNet.Common;
using Twilio.TwiML.Voice;

namespace TwilioInitilizationWithMvc.Controllers
{
    //"+15107687251"
    public class HomeController : TwilioController
    {
        private readonly MySQLDBClass dbmysql;
        const string accountSid = "ACc5375fdc5addc508794324784dd0b790";
        const string authToken = "43744100b0ca18503eb495a5394b4ef8";
        private readonly ApplicationDbContext context;
        public HomeController()
        {
            dbmysql = new MySQLDBClass();  
            context = new ApplicationDbContext();
            
        }
        public ActionResult Index()
        {
            //string cmd = "SELECT * FROM SmsLog";
            //var dt = dbmysql.MySQLSelect(cmd);
            //var smsLog = (from DataRow dr in dt.Rows
            //              select new SmsDetail()
            //              {
            //                  StatusId= Convert.ToInt16(dr["StatusId"]),
            //                  Id =Convert.ToInt16(dr["Id"]),
            //                  SmsDeliveryTime =Convert.ToDateTime(dr["SmsDeliveryDateTime"]),
            //                  text = Convert.ToString(dr["SmsText"]),
            //                  ClientNumber=Convert.ToString(dr["ClientNumber"])

            //              }).ToList();
            var smslog = context.SmsDetail.Include(s => s.Status).ToList();
            return View(smslog);
        }
        public ActionResult SmsSender()
        {
            return View();
        }
        public ActionResult CallMaker()
        {
            return View();
        }
        public ActionResult Calling()
        {
            return View();
        }
        public ActionResult MakeCall(SmsFormViewModel callInfo)
        {
            

            TwilioClient.Init(accountSid, authToken);

            var statusCallbackEvent = new List<string> {
                "completed",
                "ringing"
            };



            var To   = new PhoneNumber(callInfo.number);
            var From = new PhoneNumber("+15107687251");
            var call = CallResource.Create(
                    method: Twilio.Http.HttpMethod.Get,
                    statusCallback: new Uri("https://5bd43ce7.ngrok.io/Api/CallStatus/GetCallStatus"),
                    statusCallbackEvent: statusCallbackEvent,
                    statusCallbackMethod: Twilio.Http.HttpMethod.Post,
                    to:To,
                    from:From,
                    url: new Uri("http://demo.twilio.com/docs/voice.xml"));
                   // SaveCallsToDbAccordingToStatus();
            return RedirectToAction("Calling", "Home");
        }
        public ActionResult SendMessage(SmsFormViewModel smsInfo)
        {
            TwilioClient.Init(accountSid, authToken);

            var message = MessageResource.Create(
                body: smsInfo.smsText,
                from: new Twilio.Types.PhoneNumber("+15107687251"),
                to: new Twilio.Types.PhoneNumber(smsInfo.number)
            );
            var detail = new SmsDetail() {
                ClientNumber = smsInfo.number,
                text = smsInfo.smsText,
                SmsDeliveryTime = DateTime.Now,
                StatusId=1
            };
            context.SmsDetail.Add(detail);
            context.SaveChanges();
            return RedirectToAction("Index","Home");
        }
        public ActionResult CallLog()
        {
           // SaveCallsToDbAccordingToStatus();
            TwilioClient.Init(accountSid, authToken);
            var callsToShow = CallResource.Read();
            return View(callsToShow.ToList());
        }
        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";
            return View();
        }
        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";
            return View();
        }
        public void SaveCallsToDbAccordingToStatus()
        {
            TwilioClient.Init(accountSid, authToken);

            var QueuedCalls = CallResource.Read(
                        status: CallResource.StatusEnum.Queued
                        );
            foreach (var call in QueuedCalls)
            {
                if (call.Status.ToString() == "queued")
                {
                    var callDetailRinging = new QueuedCalls()
                    {
                        CallTo = call.To,
                        CallSID = call.Sid,
                        CallFrom = call.From,
                        DateCreated = call.DateCreated,
                        DateUpdated = call.DateUpdated,
                        Duration = call.Duration,
                        StartTime = call.StartTime,
                        EndTime = null,
                        Direction = call.Direction,
                        Status = call.Status.ToString()
                    };
                    context.QueuedCalls.Add(callDetailRinging);
                }

            }
            /////////////////////////////////////////////////////////////////
            var calls = CallResource.Read(
                        status: CallResource.StatusEnum.Ringing
                        );
            foreach (var call in calls)
            {
                if (call.Status.ToString() == "ringing")
                {
                    var callDetailRinging = new RingingCalls()
                    {
                        CallTo = call.To,
                        CallSID = call.Sid,
                        CallFrom = call.From,
                        DateCreated = call.DateCreated,
                        DateUpdated = call.DateUpdated,
                        Duration = call.Duration,
                        StartTime = call.StartTime,
                        EndTime = null,
                        Direction = call.Direction,
                        Status = call.Status.ToString()
                    };
                    //var cmd = "INSERT INTO RingingCalls (CallSID,CallTo,CallFrom,Direction,DateCreated,Status)VALUES('" + callDetailRinging.CallSID + "','" + callDetailRinging.CallTo + "','" + callDetailRinging.CallFrom + "','" + callDetailRinging.Direction + "','" + callDetailRinging.DateCreated + "','" + callDetailRinging.Status + "'); ";
                    //dbmysql.MySQLInsert(cmd);
                    string IP = Request.UserHostAddress;
                    var cmd = "INSERT INTO live_channels VALUES('" + 1001 + "','"+ IP +"','" + callDetailRinging.Direction + "','" + 1001 + "','" + callDetailRinging.CallTo + "');";
                    dbmysql.MySQLInsert(cmd);


                    context.RingingCalls.Add(callDetailRinging);
                }
                else
                {
                    var callDetail = new CallDetail()
                    {
                        CallTo = call.To,
                        CallSID = call.Sid,
                        CallFrom = call.From,
                        DateCreated = call.DateCreated,
                        DateUpdated = call.DateUpdated,
                        Duration = call.Duration,
                        StartTime = call.StartTime,
                        EndTime = call.EndTime,
                        Direction = call.Direction,
                        Status = call.Status.ToString()
                    };
                    context.CallDetail.Add(callDetail);
                }
            }

            context.SaveChanges();
        }

       
    }
}