﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MySql;
using MySql.Data.MySqlClient;
using MySql.Data;
using System.Data;

namespace TwilioInitilizationWithMvc.Controllers
{
    public class MySQLDBClass
    {
        MySqlDataAdapter da;
        MySqlConnection con;
        DataTable dt;
      public MySQLDBClass()
        {
            dt = new DataTable();
            da = new MySqlDataAdapter();
            con = new MySqlConnection();
            con.ConnectionString = "Server=192.168.2.25;Database=asterisk;Uid=root;Pwd=catcos123;";
        }
        


        public DataTable MySQLSelect(string command)
        {            
            MySqlCommand comm = new MySqlCommand(command, con);
            con.Open();
            da.SelectCommand = comm;
            da.Fill(dt);
            con.Close();
            con.Dispose();
            return dt;
        }


        public void MySQLInsert(string command)
        {
                       
            MySqlCommand comm = new MySqlCommand(command, con);
            con.Open();
            comm.ExecuteNonQuery();
            con.Close();
            con.Dispose();

        }


        public void MySQLUpdate(string command)
        {
           
            MySqlCommand comm = new MySqlCommand(command, con);
            con.Open();
            comm.ExecuteNonQuery();
            con.Close();
            con.Dispose();

        }


        public void MySQLDelete(string command)
        {

            MySqlCommand comm = new MySqlCommand(command, con);
            con.Open();
            comm.ExecuteNonQuery();
            con.Close();
            con.Dispose();

        }

    }
}