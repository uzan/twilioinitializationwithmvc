﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace TwilioInitilizationWithMvc.Controllers
{
    public class AssingmentCallBackController : ApiController
    {
        public static HttpListenerResponse SendResponse(HttpListenerContext ctx)
        {
            HttpListenerRequest request = ctx.Request;
            HttpListenerResponse response = ctx.Response;

            String endpoint = request.RawUrl;

            if (endpoint.EndsWith("assignment_callback"))
            {
                response.StatusCode = (int)HttpStatusCode.OK;
                response.ContentType = "application/json";
                response.StatusDescription = "{}";
                return response;
            }
            response.StatusCode = (int)HttpStatusCode.OK;
            return response;
        }
    }
}
