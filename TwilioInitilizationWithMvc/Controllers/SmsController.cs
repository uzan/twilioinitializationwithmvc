﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Twilio.AspNet.Common;
using Twilio.AspNet.Mvc;
using Twilio.TwiML;
using TwilioInitilizationWithMvc.Models;

namespace TwilioInitilizationWithMvc.Controllers
{
    public class SmsController : TwilioController
    {
        private ApplicationDbContext context;
        public SmsController()
        {
            context = new ApplicationDbContext();
        }
        public TwiMLResult Index(SmsRequest incomingMessage)
        {
            //var detail = new SmsDetail() {
            //    text = incomingMessage.Body,

            //};
            var messagingResponse = new MessagingResponse();
            messagingResponse.Message("The copy cat says: " +
                                      incomingMessage.Body);

            var detail = new SmsDetail()
            {
                ClientNumber = incomingMessage.From,
                text = incomingMessage.Body,
                SmsDeliveryTime = DateTime.Now,
                StatusId = 2
            };
            context.SmsDetail.Add(detail);
            context.SaveChanges();
           
            
            return TwiML(messagingResponse);
        }
    }
}