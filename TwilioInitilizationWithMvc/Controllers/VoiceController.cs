﻿using System;
using System.Collections.Generic;
using System.Linq;

using System.Web;
using System.Web.Mvc;
using Twilio;
using Twilio.AspNet.Common;
using Twilio.AspNet.Mvc;
using Twilio.Rest.Api.V2010.Account;
using Twilio.Rest.Api.V2010.Account.Queue;
using Twilio.TwiML;
using Twilio.TwiML.Voice;
using Twilio.Types;


namespace TwilioInitilizationWithMvc.Controllers
{
    public class VoiceController : TwilioController
    {
        private readonly MySQLDBClass dbmysql;
        public VoiceController()
        {
            dbmysql = new MySQLDBClass();
        }
       
        const string accountSid = "ACc5375fdc5addc508794324784dd0b790";
        const string authToken = "43744100b0ca18503eb495a5394b4ef8";
       
        [HttpPost]
        public TwiMLResult Index(VoiceRequest request)
        {
            string IP = Request.UserHostAddress;
           // var From = "+15107687251";
            var response = new VoiceResponse();
            if (!string.IsNullOrEmpty(request.To))
            {
                if (request.CallStatus == "ringing")
                {
                    var cmmd = "INSERT INTO live_channels VALUES('" + request.To + "','" + IP + "','" + "outgoing" + "','" + 1001 + "','" + request.To + "');";
                    dbmysql.MySQLInsert(cmmd);
                }
                //+17739662357
                var number = new Dial(callerId: "+15107687251", action: new Uri("https://29ef55b0.ngrok.io/Api/CallStatus/GetCallStatusOutbound")
                    ,method: Twilio.Http.HttpMethod.Post);
               number.Number(request.To);
                response.Append(number);
            }
            else 
            {
                const string accountSid = "ACc5375fdc5addc508794324784dd0b790";
                const string authToken = "43744100b0ca18503eb495a5394b4ef8";

                TwilioClient.Init(accountSid, authToken);
                var queue = QueueResource.Fetch(pathSid: "QUcea7afbe3ee14c8588761cbb68dd1fb7");
                Console.WriteLine(queue.CurrentSize);

                var queueMember = MemberResource.Read(
                    pathQueueSid: "QUcea7afbe3ee14c8588761cbb68dd1fb7",
                            limit: 1
                            );
                
                foreach (var callin in queueMember)
                {
                    if (callin.Position == 1)
                    {
                        var call = CallResource.Fetch(pathSid: callin.CallSid);
                        var cmmd = "INSERT INTO live_channels VALUES('" + 1001 + "','" + IP + "','" + "incoming" + "','" + 1001 + "','" + call.From + "');";
                        dbmysql.MySQLInsert(cmmd);
                        break;
                    }
                    else
                        continue;
                }

                       
                response.Say("Thanks for Answering!");
                var dial = new Dial();
                dial.Queue("Complain", url: new Uri("aboutToConnect", UriKind.Relative)); //, url: new Uri("about_to_connect", UriKind.Relative)
                response.Append(dial);
            }

            //TwiMLResult rs =
            return TwiML(response); //Content(response.ToString(), "text/xml");
        }

        [HttpPost]
        public TwiMLResult aboutToConnect()
        {
            var response = new VoiceResponse();
            response.Say("You will now be connected to an agent.");
            return TwiML(response);
        }
    }
}
//TwilioClient.Init(accountSid, authToken);

//var statusCallbackEvent = new List<string> {
//    "completed",
//    "ringing"
//};
//var To= new PhoneNumber(request.To);
//var From = "+15107687251";
//var call = CallResource.Create(
//        method: Twilio.Http.HttpMethod.Get,
//        statusCallback: new Uri("https://5bd43ce7.ngrok.io/Api/CallStatus/GetCallStatus"),
//        statusCallbackEvent: statusCallbackEvent,
//        statusCallbackMethod: Twilio.Http.HttpMethod.Post,
//        to: To,
//        from: From);
//response.Append(call);

//    var To = new PhoneNumber(callInfo.number);
//    var From = new PhoneNumber("+15107687251");
//    var call = CallResource.Create(
//            method: Twilio.Http.HttpMethod.Get,
//            statusCallback: new Uri("https://5bd43ce7.ngrok.io/Api/CallStatus/GetCallStatus"),
//            statusCallbackEvent: statusCallbackEvent,
//            statusCallbackMethod: Twilio.Http.HttpMethod.Post,
//            to: To,
//            from: From,
//            url: new Uri("http://demo.twilio.com/docs/voice.xml"));