﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Twilio.AspNet.Common;
using Twilio.AspNet.Mvc;
using Twilio.TwiML;
using Twilio.TwiML.Voice;

namespace TwilioInitilizationWithMvc.Controllers
{
    public class IVRController : TwilioController
    {
        private readonly MySQLDBClass dbmysql;
        public IVRController()
        {
            dbmysql = new MySQLDBClass();
        }
        [HttpPost]
        public TwiMLResult dosranumber()
        {
            var response = new VoiceResponse();
            var dial = new Dial();
            dial.Client("Uzain");
            response.Append(dial);
            return TwiML(response);
        }
        [HttpPost]
        public TwiMLResult Welcome(VoiceRequest request)
        {
           
        //string IP = Request.UserHostAddress;
        //    var cmmd = "INSERT INTO live_channels VALUES('" + 1001 + "','" + IP + "','" + "incoming" + "','" + 1001 + "','" + request.From + "');";
        //    dbmysql.MySQLInsert(cmmd);
            var response = new VoiceResponse();
            var gather = new Gather(action: Url.ActionUri("Show", "IVR"), numDigits: 1);
            gather.Say("Thank you for calling Catcos Service - the " +
                       "adventurous work first choice in world of technology. " +
                       "Press 1 for directions, press 2 to make a call.");
            response.Append(gather);

            return TwiML(response);
        }
        [HttpPost]
        public ActionResult Show(string digits)
        {
            var selectedOption = digits;
            var optionActions = new Dictionary<string, Func<ActionResult>>()
            {
                {"1", ReturnInstructions},
                {"2", Planets}
            };

            return optionActions.ContainsKey(selectedOption) ?
                optionActions[selectedOption]() : RedirectWelcome();
        }

        private TwiMLResult ReturnInstructions()
        {
            var response = new VoiceResponse();
            response.Say("To get to your extraction point, get on your bike and go down " +
                         "the street. Then Left down an alley. Avoid the police cars. Turn left " +
                         "into an unfinished housing development. Fly over the roadblock. Go " +
                         "passed the moon. Soon after you will see your mother ship.",
                         voice: "alice", language: "en-GB");

            response.Say("Thank you for calling the E.T. Phone Home Service - the " +
                         "adventurous alien's first choice in intergalactic travel. Good bye.");

            response.Hangup();

            return TwiML(response);
        }

        private TwiMLResult Planets()
        {
            var response = new VoiceResponse();
            var gather = new Gather(action: Url.ActionUri("Interconnect", "IVR"), numDigits: 1);
            gather.Say("To call sir Faisal, press 2." +
                     "go back to the main menu, press the star key ",
                     voice: "alice", language: "en-GB", loop: 3);
            response.Append(gather);

            return TwiML(response);
        }
        public ActionResult RedirectWelcome()
        {
            return RedirectToAction("Welcome", "IVR");
        }

        [HttpPost]
        public ActionResult Interconnect(string digits)
        {
            var userOption = digits;
            var optionPhones = new Dictionary<string, string>
            {
                {"2", "Uzain"},
                {"3", "+923032878711"},
                {"4", "+17739662357"}

            };
            return optionPhones.ContainsKey(userOption)
                ? Dial(optionPhones[userOption]) : RedirectWelcome();
        }

        private TwiMLResult Dial(string phoneNumber)
        {
            var response = new VoiceResponse();
            if (phoneNumber != "Uzain")
            {
                response.Dial(phoneNumber);
            }
            else
            {
                response.Enqueue("Complain", waitUrl: new Uri("MyMusic", UriKind
                    .Relative));
            }
            return TwiML(response);
        }

        [HttpPost]
        public TwiMLResult MyMusic()
        {
            var response = new VoiceResponse();
            response.Play(new Uri("http://com.twilio.sounds.music.s3.amazonaws.com/MARKOVICHAMP-Borghestral.mp3"));

            return TwiML(response);
        }
    }
}


//  response.Append(response);

//var dial = new Dial();
//dial.Client("Uzain");

//var dial = new Dial();
//dial.Client("joey");
