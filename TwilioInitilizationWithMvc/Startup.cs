﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(TwilioInitilizationWithMvc.Startup))]
namespace TwilioInitilizationWithMvc
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
