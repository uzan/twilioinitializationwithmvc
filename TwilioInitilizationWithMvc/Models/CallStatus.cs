﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TwilioInitilizationWithMvc.Models
{
    public enum CallStatus
    {
        queued=0,
        ringing=1,
        in_progress=2,
        canceled=3,
        completed=4,
        failed=5,
        busy=6,
        no_answer=7

        // queued, ringing, in-progress, canceled, completed, failed, busy or no-answer
    }
}