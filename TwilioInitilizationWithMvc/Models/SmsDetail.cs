﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TwilioInitilizationWithMvc.Models
{
    public class SmsDetail
    {
        public int Id { get; set; }
        public string text { get; set; }
        public string ClientNumber { get; set; }
        public Status Status { get; set; }
        public int StatusId { get; set; }
        public DateTime SmsDeliveryTime { get; set; }
    }
}