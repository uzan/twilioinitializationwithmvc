﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TwilioInitilizationWithMvc.Models
{
    public class QueuedCalls
    {
        public int Id { get; set; }
        public string CallSID { get; set; }
        public string CallTo { get; set; }
        public string CallFrom { get; set; }
        public string Direction { get; set; }
        public string Duration { get; set; }
        public DateTime? DateCreated { get; set; }
        public DateTime? DateUpdated { get; set; }
        public DateTime? StartTime { get; set; }
        public DateTime? EndTime { get; set; }
        public string Status { get; set; }
    }
}