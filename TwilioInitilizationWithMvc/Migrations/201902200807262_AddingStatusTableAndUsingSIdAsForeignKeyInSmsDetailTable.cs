namespace TwilioInitilizationWithMvc.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddingStatusTableAndUsingSIdAsForeignKeyInSmsDetailTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Status",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.SmsDetails", "StatusId", c => c.Int(nullable: false));
            CreateIndex("dbo.SmsDetails", "StatusId");
            AddForeignKey("dbo.SmsDetails", "StatusId", "dbo.Status", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.SmsDetails", "StatusId", "dbo.Status");
            DropIndex("dbo.SmsDetails", new[] { "StatusId" });
            DropColumn("dbo.SmsDetails", "StatusId");
            DropTable("dbo.Status");
        }
    }
}
