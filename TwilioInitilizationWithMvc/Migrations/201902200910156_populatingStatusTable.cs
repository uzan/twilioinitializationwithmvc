namespace TwilioInitilizationWithMvc.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class populatingStatusTable : DbMigration
    {
        public override void Up()
        {
            Sql("INSERT INTO Status (Name) VALUES ('Sent')");
            Sql("INSERT INTO Status (Name) VALUES ('Recived')");
        }
        
        public override void Down()
        {
            Sql("DELETE FROM Status");
        }
    }
}
