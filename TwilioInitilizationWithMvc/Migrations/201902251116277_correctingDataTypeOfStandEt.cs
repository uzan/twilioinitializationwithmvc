namespace TwilioInitilizationWithMvc.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class correctingDataTypeOfStandEt : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.CallDetails", "StartTime", c => c.DateTime());
            AlterColumn("dbo.CallDetails", "EndTime", c => c.DateTime());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.CallDetails", "EndTime", c => c.String());
            AlterColumn("dbo.CallDetails", "StartTime", c => c.String());
        }
    }
}
