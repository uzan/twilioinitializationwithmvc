namespace TwilioInitilizationWithMvc.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddingcallStatus : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.CallDetails", "Status", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.CallDetails", "Status");
        }
    }
}
