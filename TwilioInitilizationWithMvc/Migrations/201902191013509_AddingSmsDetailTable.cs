namespace TwilioInitilizationWithMvc.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddingSmsDetailTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.SmsDetails",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        text = c.String(),
                        ClientNumber = c.String(),
                        SmsDeliveryTime = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.SmsDetails");
        }
    }
}
