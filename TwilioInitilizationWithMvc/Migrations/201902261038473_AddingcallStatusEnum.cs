namespace TwilioInitilizationWithMvc.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddingcallStatusEnum : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.CallDetails", "CallStatus", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.CallDetails", "CallStatus");
        }
    }
}
