namespace TwilioInitilizationWithMvc.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class removingcallStatusEnum : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.CallDetails", "CallStatus");
        }
        
        public override void Down()
        {
            AddColumn("dbo.CallDetails", "CallStatus", c => c.Int(nullable: false));
        }
    }
}
