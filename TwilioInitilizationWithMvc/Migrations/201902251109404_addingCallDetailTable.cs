namespace TwilioInitilizationWithMvc.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addingCallDetailTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.CallDetails",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CallTo = c.String(),
                        CallFrom = c.String(),
                        Direction = c.String(),
                        Duration = c.String(),
                        DateCreated = c.DateTime(),
                        DateUpdated = c.DateTime(),
                        StartTime = c.String(),
                        EndTime = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.CallDetails");
        }
    }
}
