namespace TwilioInitilizationWithMvc.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddingQueuedTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.QueuedCalls",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CallSID = c.String(),
                        CallTo = c.String(),
                        CallFrom = c.String(),
                        Direction = c.String(),
                        Duration = c.String(),
                        DateCreated = c.DateTime(),
                        DateUpdated = c.DateTime(),
                        StartTime = c.DateTime(),
                        EndTime = c.DateTime(),
                        Status = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.QueuedCalls");
        }
    }
}
